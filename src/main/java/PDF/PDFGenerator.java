package PDF;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Genera un archivo PDF en memoria o fisico para ser impreso
 */
public class PDFGenerator {

    private static Logger logger = LoggerFactory.getLogger(PDFGenerator.class);

    public static void generateDocument() throws IOException {
        logger.info("Generando pdf");
        boolean fileTemp = false;
        PdfDocument pdfDocument;

        if (fileTemp) {
            File dest = File.createTempFile("Remision",".pdf",null);
            pdfDocument = new PdfDocument(new PdfWriter(dest));
        } else {
            String dest = "sample.pdf";
            pdfDocument = new PdfDocument(new PdfWriter(dest));
        }


        // Adding a new page
        pdfDocument.addNewPage();

        // Creating a Document
        Document document = new Document(pdfDocument, PageSize.A4);

        // Closing the document
        document.close();
        //dest.deleteOnExit();
    }

}
