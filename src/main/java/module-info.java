module remissionGenerator {
    requires javafx.fxml;
    requires javafx.controls;
    requires kernel;
    requires layout;
    requires slf4j.api;

    opens app to javafx.fxml;
    exports app;
}